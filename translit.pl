#!/usr/bin/perl -w

#use locale;
#use POSIX qw(locale_h);
#setlocale(LC_ALL,"ru_RU.KOI8-R");
#use Data::Dumper;

my $trans = {
	'�' => [ 'a' ],
	'�' => [
		'b',
#		'6'
		],
	'�' => [
		'v',
#		'w'
		],
	'�' => [ 'g' ],
	'�' => [ 'd' ],
	'�' => [
		'e',
#		'ie',
#		'je',
#		'ye'
		],
	'�' => [
		'e',
		'io',
		'jo',
		'yo'
		],
	'�' => [
		'g',
		'j',
		'zh',
#		'*',
#		'>|<',
#		'z'
		],
	'�' => [
		'z',
#		'3'
		],
	'�' => [
		'i',
#		'j',
#		'u'
		],
	'�' => [
		'i',
		'j',
#		'u',
		'y',
		'' ],
	'�' => [
#		'c',
		'k'
		],
	'�' => [
		'l',
#		'Jl'
		],
	'�' => [ 'm' ],
	'�' => [ 'n' ],
	'�' => [
		'o',
#		'0'
		],
	'�' => [
#		'n',
		'p'
		],
	'�' => [ 'r' ],
	'�' => [
		's',
#		'c'
		],
	'�' => [
#		'm',
		't'
		],
	'�' => [
		'u',
#		'y'
		],
	'�' => [ 'f' ],
	'�' => [
		'h',
#		'kh',
#		'x'
		],
	'�' => [
		'c',
#		'cz',
#		'tc',
		'ts'
		],
	'�' => [
		'ch',
		'4'
		],
	'�' => [
		'sh',
#		'6',
		'w'
		],
	'�' => [
		'w',
		'sh',
#		'shh',
#		'shch'
		],
	'�' => [
		'"',
		'\'',
#		'``',
#		'y',
#		'j',
#		'#',
		''
		],
	'�' => [
		'y',
		'i',
#		'q',
#		'bl'
		],
	'�' => [
		'\'',
#		'`',
#		'y',
#		'j',
		'b',
		''
		],
	'�' => [
		'e',
#		'e\'',
#		'eh'
		],
	'�' => [
		'u',
#		'iu',
		'ju',
		'yu'
		],
	'�' => [
#		'a',
#		'ea',
#		'ia',
#		'ja',
#		'q',
#		'9',
#		'9I',
#		'9l',
		'ya'
		],
};

sub mutate {
	my $mutate = shift;
	my $head   = shift;

	if ($mutate eq '') {
		print "$head\n";
		return;
	}

	my $first = substr $mutate, 0, 1;
	my $rest  = substr $mutate, 1;
	$head = '' if !defined($head);

	if (exists($trans->{$first})) {
		for ( @{$trans->{$first}} ) {
			mutate($rest, "${head}$_");
		}
	} else {
		mutate($rest, "${head}$first");
	}

}

while (<>) {
	chomp;
	mutate($_);
}
