#!/usr/bin/perl -w

use Getopt::Long;
no warnings 'syntax';

my @keyboard = (
[      qw(` 1 2 3 4 5 6 7 8 9 0 - = )		],
[ undef, qw(q w e r t y u i o p [ ])  		],
[ undef, qw(a s d f g h j k l ; '), undef	],
[ undef, qw(z x c v b n m , . /), undef, undef	]
);

#
#  . + +
#  + X +
#  + + .
#

sub make_step($$$$);

sub make_step($$$$)
{
	my $chain = shift;
	my $x = shift;
	my $y = shift;
	my $steps = shift;

	$chain .= $keyboard[$x]->[$y];
	$steps--;
	unless ($steps) {
		print "$chain\n";
		return;
	}

	if ($x>0) # move up
	{
		make_step($chain, $x-1, $y, $steps) if defined $keyboard[$x-1]->[$y];
		make_step($chain, $x-1, $y+1, $steps) if defined $keyboard[$x-1]->[$y+1];
		make_step($chain, $x-1, $y-1, $steps) if ($y>0 && defined $keyboard[$x-1]->[$y-1]);
	} # if ($x>0)
	if ($y>0) # move left
	{
		make_step($chain, $x, $y-1, $steps) if defined $keyboard[$x]->[$y-1];
		make_step($chain, $x+1, $y-1, $steps) if defined $keyboard[$x+1]->[$y-1];
	} # if ($y>0)

	make_step($chain, $x, $y+1, $steps) if defined $keyboard[$x]->[$y+1];
	make_step($chain, $x+1, $y, $steps) if defined $keyboard[$x+1]->[$y];
	make_step($chain, $x+1, $y+1, $steps) if defined $keyboard[$x+1]->[$y+1];
}

my $min = 1;
my $max = 100;
GetOptions( "min=i" => \$min, "max=i" => \$max );

for (my $steps=$min; $steps <= $max; $steps++)
{
	for (my $x=0; $x<@keyboard; $x++)
	{
		for (my $y=0; $y<@{ $keyboard[$x] }; $y++)
		{
			make_step('', $x, $y, $steps) if defined $keyboard[$x]->[$y];
		}
	}
}
