#!/usr/bin/perl -w

#use locale;
#use POSIX qw(locale_h);
#setlocale(LC_ALL,"ru_RU.KOI8-R");
#use Data::Dumper;

my $trans = {
	'�' => [ 'f' ],
	'�' => [ ',' ],
	'�' => [ 'd' ],
	'�' => [ 'u' ],
	'�' => [ 'l' ],
	'�' => [ 't' ],
	'�' => [ '`' ],
	'�' => [ ';' ],
	'�' => [ 'p' ],
	'�' => [ 'b' ],
	'�' => [ 'q' ],
	'�' => [ 'r' ],
	'�' => [ 'k' ],
	'�' => [ 'v' ],
	'�' => [ 'y' ],
	'�' => [ 'j' ],
	'�' => [ 'g' ],
	'�' => [ 'h' ],
	'�' => [ 'c' ],
	'�' => [ 'n' ],
	'�' => [ 'e' ],
	'�' => [ 'a' ],
	'�' => [ '[' ],
	'�' => [ 'w' ],
	'�' => [ 'x' ],
	'�' => [ 'i' ],
	'�' => [ 'o' ],
	'�' => [ ']' ],
	'�' => [ 's' ],
	'�' => [ 'm' ],
	'�' => [ '\'' ],
	'�' => [ '.' ],
	'�' => [ 'z' ],
};

sub mutate {
	my $mutate = shift;
	my $head   = shift;

	if ($mutate eq '') {
		print "$head\n";
		return;
	}

	my $first = substr $mutate, 0, 1;
	my $rest  = substr $mutate, 1;
	$head = '' if !defined($head);

	if (exists($trans->{$first})) {
		for ( @{$trans->{$first}} ) {
			mutate($rest, "${head}$_");
		}
	} else {
		mutate($rest, "${head}$first");
	}

}

while (<>) {
	chomp;
	mutate($_);
}
