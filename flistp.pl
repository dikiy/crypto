#!/usr/bin/perl -w

die "Wrong option" if grep { !/^[+-t](.+)$/i } @ARGV;

my $threshold = 1;

use DBI;
my $dbh = DBI->connect("dbi:SQLite:dbname=./temp.db","","") or die();
$dbh->do("PRAGMA cache_size = 400000");
$dbh->do("PRAGMA synchronous = OFF");
$dbh->{AutoCommit} = 0;
$dbh->{sqlite_unicode} = 1;

$dbh->do(q{
  CREATE TABLE IF NOT EXISTS wl (
    word VARCHAR(16) PRIMARY KEY,
    freq INT
    )
});

my $add = $dbh->prepare(q{
    INSERT OR REPLACE INTO wl (word, freq) VALUES (?1, ?2)
});

my $get = $dbh->prepare(q{
    SELECT freq FROM wl WHERE word=?1
});

my $del = $dbh->prepare(q{
    DELETE FROM wl WHERE word=?
});

foreach (@ARGV) {
	m/^t(\d+)$/i && do { $threshold = $1 };
	m/^([+-])(.+)$/ && do {
		open F, "<$2" or die ($!);
		my $o = $1;
		my $c = 0;
		while (<F>) {
			chomp;
			next unless m/^(.*)\t(\d+)$/;
			if ( $o eq "+" ) {
				$get->execute($1);
				if (my @v = $get->fetchrow_array()) {
				    $add->execute($1, $2 + $v[0]);
				} else {
				    $add->execute($1, $2);
				}
			}
			$del->execute($1) if $o eq "-";
			$c++;
			print STDERR "." if ($c % 1000 ==0);
			do {print STDERR "$c"; $dbh->commit} if ($c % 100000 ==0);
		}
		close F;
	};
}

$dbh->commit;
END {$dbh->commit}

my $getall = $dbh->prepare(q{
    SELECT * FROM wl
});

$getall->execute();
while ( @row = $getall->fetchrow_array ) {print "$row[0]\t$row[1]\n"}
