#!/usr/bin/perl -w

die "Wrong option" if grep { !/^[+-](.+)$/ } @ARGV;

my %res;
foreach (@ARGV) {
	m/^([+-])(.+)$/ && do {
		open F, "<$2" or die ($!);
		my %file = map { $_ => 1 } grep {chomp && !/^$/} <F>;
		close F;

		@res{keys %file} = 1 if $1 eq "+";
		%res = map { $_ => 1 } grep {not $file{$_}} keys %res if $1 eq "-";
	};
}

foreach (sort keys %res) { print "$_\n" }
