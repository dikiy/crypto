#!/usr/bin/perl


use Getopt::Long;
use utf8;
no warnings 'layer';
binmode(STDOUT, ":encoding(UTF-8)");

my $enc = "utf-8";
#my $enc_out = "ru_RU.koi8-r";
my $test;
my $path;
GetOptions (
	    "e|encoding:s" => \$enc,
	    "p|path:s" => \$path,
	    "t|test" => \$test,
	) or exit;

if (!defined $path) { die "Path not specified!"; }

$|++;
$/ = undef;

my %H;
my $fc, $ftot;

if (-f $path) {
    do_file($path);
} elsif (-d $path) {
    traverse($path)
}

sub traverse {
	my $dir = shift;
	opendir (DIR, $dir) or do {
		print STDERR "Can't open $dir: $!\n"; return 0; };
	my @files = grep { $_ !~ m!^\.\.?$! } readdir (DIR);
	$ftot += @files;
	foreach my $file (@files) {
		next if $file =~ m/\.(jpe?g|png|js|gif|swf|css|pdf)/i;
		last if $test and $fc > 100;
		do_file("$dir/$file") if -f "$dir/$file";
		traverse("$dir/$file") if -d "$dir/$file";
	}
}

sub do_file {
	my $f = shift;

#	print "$fc/$ftot\t$f\n";
	open F, "-|", "links -html-assume-codepage $enc -codepage utf-8 -dump '$f'";
	while (<F>) {
		chomp;
		s/\xD0\x88/\xd1\x91/g; # fix for wrong "YO"
		s/\xC3\xA1/\xd0\xb0/g; # fix for "A" with acute
		s/\xC3\xA8/\xd0\xb5/g; # fix for "E" with grave
		s/\xC3\xA9/\xd0\xb5/g; # fix for "E" with acute
		s/\xC3\xB3/\xd0\xbe/g; # fix for "O" with acute
		s/\xC3\xBA/\xd0\xb8/g; # fix for "O" with acute
		utf8::decode($_);
		#print "###$_";
		s/\-\r?\n\s*//mg;
		s/(\w{5,15})/
			#print "|$1|";
			$H{lc $1}++;
			$1;
		/geu;
#		print "====>$_\n";
	}

	$fc++;
	if ($fc%1000==0) { print "$fc"}
	elsif ($fc%100==0) { print "."}
}

print "Dumping wordlist\n";
open D1, ">wordlist1";
foreach $key (sort keys %H) {
	print D1 "$key\n";
}
close D1;
open D2, ">wordlist2";
foreach $key (sort {$H{$b} <=> $H{$a}} keys %H) {
	print D2 "$key\t$H{$key}\n";
}
close D2;
